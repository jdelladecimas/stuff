﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace MatIconTransformer
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Invalid args");
                return;
            }

            string path = args[0];

            if (!Directory.Exists(path))
            {
                Console.WriteLine("Wrong path");
            }
            Console.WriteLine($"The Path is {path}");
            StringBuilder sb = new StringBuilder();
            var files = Directory.GetFiles(path, "*.html", SearchOption.AllDirectories);

            Console.WriteLine($"Found {files.Length} files");

            Dictionary<string, string> newByOld = new Dictionary<string, string>()
            {
                { "done", "check" },
                { "settings", "cog" }
            };
            var regex = "(<mat-icon)(.*?)>(.*)</mat-icon>";
            foreach (string file in files)
            {
                string html = File.ReadAllText(file);
                if (html.Contains("<mat-icon"))
                {
                   MatchCollection matches = Regex.Matches(html, regex);
                   foreach(var match in matches)
                    {
                        string element = match.ToString();
                        string end = "</mat-icon>";
                        var si = element.IndexOf(">");
                        var ei = element.IndexOf(end);
                        var value = element.Substring(si+1, ei-1-si);
                        Console.WriteLine(element);
                        Console.WriteLine(value);

                        // Swap the old value for the new one.
                        if (newByOld.ContainsKey(value))
                        {
                            value = newByOld[value];
                        }

                        // add the attribute
                        string attribute = $"svgIcon=\"{value}\"";
                        // <mat-icon *ngIf="whatever">{{variable}}</mat-icon>
                        // <mat-icon 
                        // *ngIf="whatever">
                        // {{variable}}
                        // </mat-icon>
                        var first = element.Split("<mat-icon");
                        var sec = first[1].Split(">");
                        var value2 = sec[1].Split(value);
                        var thrid = element.Split("</mat-icon>");

                        var newNode = $"{first[0]} {attribute} {sec[0]} {value} {thrid}";

                        // <mat-icon svgIcon="{{variable}}" *ngIf="whatever"></mat-icon>
                        // Console write the new string you want to insert
                        // once you have proven it works, to a replace of old with new
                    }
                }

                Console.WriteLine("------------------------------------------------------");
            }
        }


    }
    //using (StreamWriter outfile = new StreamWriter(mydocpath + @"\AllTxtFiles.txt"))
    //{
    //    outfile.Write(sb.ToString());
    //}

}
